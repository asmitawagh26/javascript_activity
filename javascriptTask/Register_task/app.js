
var validname = null;
var validemail = null;
var validsalary = null;
var validskill = null;
var emp =[];

function nameValidate(empName) {
	var number_regx=/^[a-zA-Z]+$/;
	if(empName.value.length < 8 || empName.value.length>16 || empName.value.charAt(0) > 'a' || empName.value.charAt(0) > 'z') {
		
		document.getElementById("message").innerHTML="this is invalid name ";
	
	} else {
		var error=document.getElementById("message");
     	error.innerHTML="";
			validname = empName.value;
	}

}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     if(re.test(email.value.toLowerCase())){
     	var error=document.getElementById("email_message");
     	error.innerHTML="";
     	validemail = email.value;
     }
     else {
     	document.getElementById("email_message").innerHTML="Invalid email";
     }
}

function salaryValidate(salary) {
	if(salary.value<50000 || salary.value>100000) {
		document.getElementById("salary_message").innerHTML="Salary should be between(50,000-1,00,00)";
	}
	else {
			var error=document.getElementById("salary_message");
     	error.innerHTML="";
			validsalary = salary.value;
	}
}

function createObject() {
	if(validname!= null && validemail != null && validsalary != null && validskill != null){
	
	validskill = document.getElementById("skill").value;
	var register_obj = {
		name:validname,
		email:validemail,
		skill:validskill,
		salary:validsalary

	};
	console.log(register_obj);
	emp.push(register_obj);
	console.log("Emp :",emp);
	}
	else{
		alert("Please fill all the details ");
	}

}

function clearText() {
	document.getElementById("name").value="";
	document.getElementById("email").value="";
	document.getElementById("skill").value="";
	document.getElementById("salary").value="";
}